# general description of the repo
Repository for CIBR INP 598 homework week08 assignment 02
brief description: in homework-week10 there is the files for homework-week10 

# change log
* 0.0.0 initial commit
* 0.1.0 Create a text file in homework-week08 titled assignment1.sh and paste in the command you used to clone the repository. 
* 0.2.0 Create empty text files in homework-week08 titled command history.txt and ignored file.txt
* 0.3.0 Change the repository ignore file to ignore all hidden files (except the .gitignore file) and the ignored file.txt file.
* 0.4.0 Copy the homework pdf file for this week from the cibr-student-2020 direc- tory on Grace into your homework-week08 directory. Echo the BASH cp to the end assignment1.sh script
* 0.5.0 Delete the week08 homework pdf using git rm command
* 0.6.0 update README.md
* 0.7.0 Answer the following questions and paste the answers into the assignment1.sh file. When all the questions are answered, stage a commit and push your edits.

* 1.0.0 add homework-week09-Q1.pdf
* 1.1.0 add homework-week09-Q2.pdf

* 2.0.0 add homework-week10 folder; create a assignment1.sh file, release candidate, references: https://github.com/murraylab/brainsmash   
* 2.1.0 add https://github.com/murraylab/brainsmash as submodule ; Merge with the development branch ; Push to Bitbucket
* 2.2.0 release candidate
* 3.0.0 release for assignment2, adding numpy checking and input checking function
