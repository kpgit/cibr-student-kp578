# 1. Create an assignment1.sh file that launches the cowsay container.
cd /gpfs/loomis/project/cibr/cibr_kp578/homework-week11
srun --pty -A cibr -p cibr bash
singularity build lolcow.sif docker://godlovedc/lolcow

# 2. Follow the commands used in lab to generate a cow that says Assignment1.
singularity exec lolcow.sif cowsay Assignment1

# 3. Save the output in a file called assignment1_cow.txt.
singularity exec lolcow.sif cowsay Assignment1 > assignment1_cow.txt


# 4. Then create a batch_assignment1.sh script to create a SLURM batch that generated 4 images that 
# each say something different. You can use the `singularity exec lolcow.sif cowsay -l` command to 
# see the full list of images you can use. Schedule a SLURM job to generate the output. Name the output 
# assignment1_images.txt.

singularity exec lolcow.sif cowsay -l
vim batch_assignment1.sh
#!/bin/bash

#SBATCH --job-name=cibr_kp578_container
#SBATCH --output=assignment1_images.txt
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --partition=cibr
#SBATCH --account=cibr
#SBATCH --time=10:00

echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay I am CIBR cow!
echo ""
echo ""
echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay -f meow I am a cat!
echo ""
echo ""
echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay -f ghostbusters Who you gonna call??
echo ""
echo ""
echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay -f dragon I am  dragon!
echo ""
echo ""

# 5. Run the script (comment your commands at the end of the assignment1.sh script).
sbatch batch_assignment1.sh

# 6. Copy the two bash and two text files for this assignment into your Bitbucket homework-week11 folder.

# 7. Commit and push.
git add -A
git commit -m "homework-week11 assignment1"
