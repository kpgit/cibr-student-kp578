# 1. Download the CIBR script upload.zip from the cibr-homeworks/ cibr-homework-week11 directory on Bitbucket.

# 2. Create an assignment3.sh script that launches the container and runs the pipeline on the data 
# outside of the Bitbucket repository. More specifically, run the pipeline on the dicom files from 
# the CIBR script upload.zip folder to generate nifti files.
cd "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/cibr-homework-week11"
unzip CIBR_script_upload.zip
mkdir input

docker pull scitran/dcm2nii

# copy dicom files to ./input/dicom folder using the following pythons script:
import sys,os
from glob import glob
from shutil import copyfile

folder="HCPA001"
inputFolder='./input/dicom/'

folders=os.listdir(f"{folder}/scans/")
folders=[i for i in folders if i[0]!='.']

dcms=[]
for curr_folder in folders:
    pwd=f"{folder}/scans/{curr_folder}/resources/DICOM/files/"
    dcms=dcms+[pwd+i for i in os.listdir(pwd)]

for dcm in dcms:
    dst=inputFolder+dcm.split("/")[-1]
    copyfile(dcm, dst)

# convert dicom to nifti files
docker run --rm  -ti \
    -v "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/cibr-homework-week11/input":/flywheel/v0/input \
    -v "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/cibr-homework-week11/output":/flywheel/v0/output \
    scitran/dcm2nii

# 3. Add all bash commands you used to pull and launch the container and paste them in the assignment3.sh file. 
# Note: do NOT store all the nifti or dicom files in your bitbucket repository! If you commit these files to 
# Bitbucket they will go over the size limit of the repo and you will have to purge the repo.

# 4. Next, rsync the single bash file and the dMRI_dir99_PA_38.nii.gz (should be 2.8MB) file into your 
# homework-week11 folder on Grace.
rsync ./output/20190314_154402dMRIdir99PAs038a001.nii.gz cibr_kp578@grace.hpc.yale.edu:/gpfs/loomis/project/cibr/cibr_kp578/homework-week11
rsync ./assignment3.sh cibr_kp578@grace.hpc.yale.edu:/gpfs/loomis/project/cibr/cibr_kp578/homework-week11/

# 5. Commit and push.
git add -A 
git commit -m "homework-week11 assignment3"
git push


