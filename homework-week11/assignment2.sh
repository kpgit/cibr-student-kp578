# 1. Create a docker hub account https://hub.docker.com.

# 2. Find a docker container that allows you to play the game Hangman.
# https://hub.docker.com/r/cricket/hangman

# 3. Create an assignment2.sh that allows you to launch the container and play a round. 
# Add all bash commands you used to pull and launch the container and paste them in the 
# assignment2.sh file.
docker pull cricket/hangman
sudo docker run -it --rm cricket/hangman hangman

# 4. Answer the following questions: What word does it have you guess? Did you win the round?
# enchoric
# no

# 5. Copy the single bash file for this assignment into your Bitbucket homework-week11 folder.

# 6. Commit and push.
