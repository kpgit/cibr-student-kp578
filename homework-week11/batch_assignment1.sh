#!/bin/bash

#SBATCH --job-name=cibr_kp578_container
#SBATCH --output=assignment1_images.txt
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --partition=cibr
#SBATCH --account=cibr
#SBATCH --time=10:00

echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay I am CIBR cow!
echo ""
echo ""
echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay -f meow I am a cat!
echo ""
echo ""
echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay -f ghostbusters Who you gonna call??
echo ""
echo ""
echo ""
echo ""
singularity exec /gpfs/loomis/project/cibr/cibr_kp578/homework-week11/lolcow.sif cowsay -f dragon I am  dragon!
echo ""
echo ""
