git clone https://kpgit@bitbucket.org/kpgit/cibr-student-kp578.git

cp /gpfs/loomis/project/cibr/cibr-student-2020/cibr-homeworks/cibr-homework-week08/CIBR_INP598_Week08_Homework.pdf ./


# What command would you use to determine if you have unstaged commits in your repository?
# git status

# How can you view the commit history of a Git repository?
# git log

# How do you view the version history of a file in Bitbucket?
# go to the repo on Bitbucket and click on the file and then look into the cdc00e9 to see all the commits for this file

# What’s the difference between source and diff when you view a file on bitbucket (e.g. assignment1.sh)
# source is the current code for the file and diff is the difference between the lastest commit and current code

# What does master refer to in your git repository?
# Master is the default main branch for the repo


