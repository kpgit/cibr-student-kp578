git clone git@bitbucket.org:kpgit/cibr-student-kp578.git
mkdir homework-week08
cd homework-week08
echo "git clone git@bitbucket.org:kpgit/cibr-student-kp578.git" > assignment1.sh
git add -A
git commit -m "Create a text file in homework-week08 titled assignment1.sh and paste in the command you used to clone the repository."
touch command_history.txt ignored_file.txt
nano ../.gitignore # add ignore condition
git commit -am "Change the repository ignore file to ignore all hidden files (except the .gitignore file) and the ignored file.txt file"
cp /gpfs/loomis/project/cibr/cibr-student-2020/cibr-homeworks/cibr-homework-week08/CIBR_INP598_Week08_Homework.pdf ./
echo "cp /gpfs/loomis/project/cibr/cibr-student-2020/cibr-homeworks/cibr-homework-week08/CIBR_INP598_Week08_Homework.pdf ./" >> assignment1.sh
git commit -am "Copy the homework pdf file for this week from the cibr-student-2020 direc- tory on Grace into your homework-week08 directory. Echo the BASH cp to the end assignment1.sh script."
git push
git rm CIBR_INP598_Week08_Homework.pdf
git commit -am "Delete the week08 homework pdf using git rm command"
git push
git pull
git commit -am "paste your terminal command history relevant to assignment 2 into the command history.txt file starting from the git clone command."
