# Assignment2 submission for kp578

# Assignment 2
# 1. On the development branch of your cibr-student-netid repository on Grace, create an assignment2.sh file.
git checkout development
git merge brainsmash-rc
touch homework-week10/assignment2.sh

# 2. Copy the connectFour.py and connectFourTools.py scripts in your homework-week10 directory from the cibr-student-2020 directory (located in cibr-homeworks/ cibr-homework-week10 directory.)
cd homework-week10/
cp /gpfs/loomis/project/cibr/cibr-student-2020/cibr-homeworks/cibr-homework-week10/CIBR_INP598_Week10_Lab_ConnectFour* ./

# 3. Checkout a feature branch titled feat_input.
git checkout -b feat_input

# 4. Add a function in the connectFourTools.py script to evaluate user input for values should be 0 to 6, inclusive. Merge to development.
vim CIBR_INP598_Week10_Lab_ConnectFourTools.py
# add check1_6 function in CIBR_INP598_Week10_Lab_ConnectFourTools.py
git add -A
git commit -am "Add a function in the connectFourTools.py script to evaluate user input for values should be 0 to 6, inclusive."
git push origin feat_input #HTB8Vqh8QqA3ZP4jasDn
git checkout development
git merge feat_input

# 5. Checkout a feature branch titled feat_numpy.
git checkout -b feat_numpy

# 6. Add a function to verify if numpy is installed. If importing numpy returns an error, then print Numpy is not installed and exit the script. Merge to development.
vim CIBR_INP598_Week10_Lab_ConnectFourTools.py
# add function verifyNumpy
git commit -am "verify if numpy is installed. If importing numpy returns an error, then print Numpy is not installed and exit the script."
git checkout development
git merge feat_numpy

# 7. Stage a release candidate for this code and update the appropriate markdown files. The release branch should be titled smoketest-rc.
git checkout -b smoketest-rc
vim ../README.md
# add 3.0.0 release for assignment2, adding numpy checking and input checking function
vim VERSION.md
# add 3.0.0 release for assignment2, adding numpy checking and input checking function
git commit -am "add 3.0.0 release for assignment2, adding numpy checking and input checking function"


# 8. Amend the assignment2.sh file by pasting in your assignment1 commands and question answers. Add Assignment2 submission for kp578 at the top of the file. Treat this docs update like a hotfix.
git checkout -b hotfix2
vim assignment2.sh
# add Assignment2 submission for kp578 and the commands for assignment2
git commit -am "add commands for assignment2"
git push origin hotfix2
git checkout smoketest-rc 
git merge hotfix2
git push

