# Create another directory in your Grace project folder titled homework-week10
cd /gpfs/loomis/project/cibr/cibr_kp578/homework-week08/cibr-student-kp578
mkdir homework-week10

# 1. Checkout a development branch.
git checkout -b development

# 2. Create an assignment1.sh file.
touch assignment1.sh

# Edit the README.md of your repository
vim README.md
# add a brief description of the repository, update the change log, create a versioning log, add release notes, and a references section (here add the reference for brainsmash).
# add in general description of the repo: brief description: in homework-week10 there is the files for homework-week10
# add in change log: * 2.0.0 add homework-week10 folder; create a assignment1.sh file, release candidate, references: https://github.com/murraylab/brainsmash

# 4. Generate a VERSION.md file.
touch VERSION.md

# 5. checkout a feature branch called brainsmash from the development branch titled feat_smash.
git checkout -b feat_smash

# 6. Incorporate a submodule of the https://github.com/murraylab/brainsmash repository on the feature branch.
cd /gpfs/loomis/project/cibr/cibr_kp578/homework-week08/cibr-student-kp578
git submodule add https://github.com/murraylab/brainsmash
git add .
git commit -m "cibr update kp578 add brainsmash submodule"
git push

# 7. Merge with the development branch. Push to Bitbucket.
git checkout development
git merge feat_smash
git push

# 8. Increment the version and update README.md and VERSION.md files.
vim README.md
# add * 2.1.0 add https://github.com/murraylab/brainsmash as submodule ; Merge with the development branch ; Push to Bitbucket
vim VERSION.md
# * 2.0.0 add homework-week10 folder; create a assignment1.sh file, release candidate, references: https://github.com/murraylab/brainsmash
# * 2.1.0 add https://github.com/murraylab/brainsmash as submodule ; Merge with the development branch ; Push to Bitbucket

# 9. Open a release candidate branch called brainsmash-rc.
git checkout -b brainsmash-rc

# 10. What actions can be done on the release branch?
# bug fixing, quick documentation, add tags

# 11. Increment the version and update README.md and VERSION.md files for the release candidate 1.0.
vim README.md 
# add 2.2.0 release candidate
vim VERSION.md
# add 2.2.0 release candidate

# 12. Stage commits for release and version the README.md and VERSION.md files. Make sure all branches are current on the Bitbucket repo.
git stage .
git commit -m "2.2.0 release candidate"
git push
git push origin brainsmash-rc #HTB8Vqh8QqA3ZP4jasDn
git push origin development
git push origin feat_smash

# 13. How do you make your repo public and available for researchers to use (you don’t have to implement this for the assignment)?
# A. publish the paper using this repo and attach this repo to the paper
# B. publish a method paper introducing this repo
# tweet about this repo to advertise

# 14. Amend the assignment1.sh file by pasting in your assignment1 commands and question answers. Add Assignment1 submission for < netid > at the top of the file. Treat this docs update like a hotfix.
git checkout -b hotfix
vim homework-week10/assignment1.sh
# add this file
git checkout brainsmash-rc
git merge hotfix
git push
git push origin hotfix
