* 2.0.0 add homework-week10 folder; create a assignment1.sh file, release candidate, references: https://github.com/murraylab/brainsmash
* 2.1.0 add https://github.com/murraylab/brainsmash as submodule ; Merge with the development branch ; Push to Bitbucket
* 2.2.0 release candidate
* 3.0.0 release for assignment2, adding numpy checking and input checking function
