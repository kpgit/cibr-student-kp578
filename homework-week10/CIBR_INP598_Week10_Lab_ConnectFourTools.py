# Connect 4 game - INP598 Fall 2020

# Player 1 will be X's and Player 2 will be O's.  For blank spaces,
# use an underscore.
TOKENS = {1: "X", 2: "O", 0: "_"}

def check1_6(x):
    # evaluate user input for values should be 0 to 6, inclusive
    if x>=0 and x<=6:
        return True
    else:
        return False

def verifyNumpy():
    # verify if numpy is installed. If importing numpy returns an error, then print Numpy is not installed and exit the script.
    try:
        import numpy
    except ImportError:
        print "Numpy is not installed"
        exit()

def display_board(board):
    """Print the board given by `board` to the screen."""
    ROWS, COLS = board.shape
    display = ""
    for i in range(0, board.shape[0]):
        for j in range(0, board.shape[1]):
            display += TOKENS[board[i,j]]
        display += "\n"
    print(display)


def did_somebody_win(board):
    """Check if there is a winner on `board`.  If so, return the winning player.  If not, return 0"""
    winner = 0 # If nobody wins, use this as the default
    ROWS, COLS = board.shape
    # Check for four in a row horizontally
    for r in range(0, ROWS):
        for c in range(0, COLS-3):
            if board[r,c] == board[r,c+1] == board[r,c+2] == board[r,c+3] != 0:
                winner = board[r,c]

    # Check for four in a row vertically
    for c in range(0, COLS):
        for r in range(0, ROWS-3):
            if board[r,c] == board[r+1,c] == board[r+2,c] == board[r+3,c] != 0:
                winner = board[r,c]

    # Check for four in a row horizontally like \
    for c in range(0, COLS-3):
        for r in range(0, ROWS-3):
            if board[r,c] == board[r+1,c+1] == board[r+2,c+2] == board[r+3,c+3] != 0:
                winner = board[r,c]

    # Check for four in a row horizontally like /
    for c in range(0, COLS-3):
        for r in range(ROWS-1, 3, -1):
            if board[r,c] == board[r-1,c+1] == board[r-2,c+2] == board[r-3,c+3] != 0:
                winner = board[r,c]

    return winner


def play_token(board, col, player):
    """For a given board `board`, put the token for player `player` into `col` and return the new board."""
    ROWS, COLS = board.shape
    new_board = board.copy() # Copy of the board

    # Find the first row in the given column which does not have any
    # tokens in it.
    already_played = False # Set to true when the token was placed
    for row in range(ROWS-1, -1, -1):
        if new_board[row,col] == 0:
            new_board[row,col] = player
            already_played = True
            break

    # Check to make sure that a token was placed, i.e. that
    # the play was valid
    if not already_played:
        print('Error, invalid play')

    return new_board

