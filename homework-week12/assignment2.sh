# Upload your container to to DockerHub. 
docker image ls
docker tag db7c3ba51274 kailongpeng/cibr_image:latest
docker login #username kailongpeng password
docker push kailongpeng/cibr_image:latest

# Add in details about the container: This is a container installing the environment needed to run connectFour code, installing python, numpy and other related packages.

# Paste the link in a file titled assignment2.sh: https://hub.docker.com/repository/docker/kailongpeng/cibr_image

# Append this file with the commands you would use to run the container with Docker and the commands you used to push to DockerHub.
docker container run -it --rm --name cibr_script cibr_image bash
python CIBR_INP598_Week12_Lab_ConnectFour.py
