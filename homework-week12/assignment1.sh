# Build a Dockerfile that launches the Connect4 python code (using connectFour and connectFourTools python scripts). 
# Create a file called assignment1.sh and paste all commands used to create and build the container.

## - Create a Dockerfile
cd "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/cibr-homework-week12"
vim Dockerfile

## - Inside Dockerfile
FROM python:3.7.5-slim

RUN python -m pip install \
    parse \
    realpython-reader \
    numpy

RUN mkdir /opt/DockerTest

ADD . /opt/DockerTest 

WORKDIR /opt/DockerTest

CMD ["bash"]

## - Build image
docker build -t cibr_image .
docker image ls

