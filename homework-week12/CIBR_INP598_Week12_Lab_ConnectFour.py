## - 
## -  Connect 4 game - INP598 Fall 2020
## - 
## - Created by Maxwell Shinn, Yale University 190921
## - Refactored by Amber Howell, Yale University 190923

## - notes:
## - to run on grace: 
## - srun --pty -A cibr -p cibr --time=2:30:00 --ntasks-per-node=1 --mem=8000 bash -l
## - module load Python

## - import packages 
import numpy as np

## - import tools/functions 
import os
print('os.getcwd()=',os.getcwd())
from CIBR_INP598_Week12_Lab_ConnectFourTools import display_board, did_somebody_win, play_token



## - set up board dimensions 
ROWS = 6
COLS = 7

# The current state of the board: 0 for empty space, 1 or 2 for player token
board = np.zeros((ROWS, COLS), dtype=int) 

# Main loop, this is where all the magic happens
while not did_somebody_win(board):
    # Each player takes their turn, player 1 goes first
    for player in [1, 2]:
        # Get the column from the player
        chosen_col = int(input(f'Player {player}, enter a column: '))
        # Put the token on the board
        board = play_token(board, chosen_col, player)
        # Display the board
        display_board(board)
        # Check for a winner
        if did_somebody_win(board):
            break

winner = did_somebody_win(board)
print(f"Winner is Player {winner}")
