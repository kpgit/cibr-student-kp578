# Assignment 3
# Build your container image using Singularity on Grace. Paste all commands to do so in a file titled assignment3.sh and copy into your homework-week12 folder.
ssh cibr_kp578@grace.hpc.yale.edu
cd /gpfs/loomis/project/cibr/cibr_kp578/homework-week12
srun --pty -A cibr -p cibr bash
singularity

scp "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/cibr-homework-week12/CIBR_INP598_Week12_Lab_ConnectFour.py" cibr_kp578@grace.hpc.yale.edu:/gpfs/loomis/project/cibr/cibr_kp578/homework-week12/
scp "/Volumes/GoogleDrive/My Drive/INP/CIBR/homework/cibr-homework-week12/CIBR_INP598_Week12_Lab_ConnectFourTools.py" cibr_kp578@grace.hpc.yale.edu:/gpfs/loomis/project/cibr/cibr_kp578/homework-week12/
singularity build cibr_kp578_script.sif docker://kailongpeng/cibr_image:latest
singularity exec cibr_kp578_script.sif bash 
python CIBR_INP598_Week12_Lab_ConnectFour.py
control D 
